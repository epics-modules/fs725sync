#!/usr/bin/env iocsh.bash

require(fs725sync)

## Unique IOC instance name, used for IOC stats and autosave status
#epicsEnvSet("IOCNAME", "labs-dev:time-fs725-01")
epicsEnvSet("P", "LABS-amor:time-fs725-01:")
epicsEnvSet("IPADDR", "172.30.244.228")
epicsEnvSet("PORT", "4001")
epicsEnvSet("EVG", "Amor-ICS:TS-EVG-01:")
epicsEnvSet("EVR", "LabS-Utgard-VIP:TS-EVR-04:")

iocshLoad("$(fs725_DIR)/fs725.iocsh", "P=$(P), IPADDR=$(IPADDR), PORT=$(PORT)")
iocshLoad("$(fs725sync_DIR)/fs725sync.iocsh","P=$(P),  EVR=$(EVR), EVG=$(EVG)")

iocInit()

